#!/usr/bin/env
import pygame
from  pygame.locals import*
import thorpy
import sys
import threading
import time
import random   
import numpy as np
from datetime import datetime
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import gridspec
from scipy.interpolate import CubicSpline
from scipy import stats
import helper_functions as hf

class App:

    def __init__(self): 
        self._running = True
        self._display_surf = None
        self.miss_counter = 0
        self.multi_press_counter=0

        self.red = (255,0,0)
        self.bool_stimulus = False
        self.ticker = 0.0
        self.stim_time = 0.0
        self.response_time =  0.0
        self.response_delay = 1000
        self.bool_response = False
        self.cross_flag = self.circ_flag = False
        self.execution_timer_flag = True
        self.elapsed_time = 0.0
        self.first_key_press = False
        self.score_df = pd.DataFrame(columns = ['cross_stim_time','circ_stim_time','absolute_response_time','correct_response','incorrect_response','no_response'])
        self.duration_button_flag = True

        self.absolute_file_path = '/home/rohith/Insync/rohithkarthikeyan@tamu.edu/Google Drive - Shared drives/tDCS: Closed Loop Neurotech/Data/[Pilot] Prediction Modeling/PVT/'
        # self.absolute_file_path = '/home/rohith/Documents/'
        self.current_time = time.time()
        self.rect_height_1 = 20
        self.rect_width_1 = 5

        self.rect_height_2 = self.rect_width_1
        self.rect_width_2 = self.rect_height_1
        self.min_response_delay = 1000

    def on_init(self):
   
        pygame.init()

        infoObject = pygame.display.Info()
        self.size = self.weight, self.height = infoObject.current_w, infoObject.current_h
        
        self.size = self.weight, self.height = 640,400  
       
        self.center_x = int(self.weight/2)
        self.center_y = int(self.height/2)

        # self._display_surf = pygame.display.set_mode(self.size, pygame.HWSURFACE | pygame.DOUBLEBUF)
        self._display_surf = pygame.display.set_mode((640,480), pygame.FULLSCREEN)
        self._running = True
        pygame.display.set_caption('The NeuroErgonomics Lab: Psychomotor Vigilance Test') 
        self.font = pygame.font.Font('freesansbold.ttf', 14) 
        self.text = self.font.render('', True, (255,255,255), (0,0,0)) 
        self.textRect = self.text.get_rect() 
        self.textRect.center = (self.weight - 300 , 25)  
        self.multi_press = True
 
 
    def on_event(self, event):
        
        if event.type == pygame.QUIT:
            self._running = False
        
        elif event.type == self.REFRESHEVENT:
            var = np.random.choice(('circle','cross', 'none'), p=[0.4, 0.2, 0.4])

            if var == 'circle':
                pygame.draw.circle(self._display_surf, self.red, (self.center_x,self.center_y), 10, 0)  
                pygame.display.update()              
                self.ticker = time.time()
                self.stim_time = self.ticker
                self.bool_stimulus = True
                self.circ_flag = True
                self.elapsed_time = 0.0
                self.multi_press = False
            
            elif var == 'cross':
                pygame.draw.rect(self._display_surf, (211,211,211), (self.weight/2-(self.rect_height_1/2),self.height/2-(self.rect_width_1/2),self.rect_height_1,self.rect_width_1))
                pygame.draw.rect(self._display_surf, (211,211,211), (self.weight/2-(self.rect_height_2/2),self.height/2-(self.rect_width_2/2),self.rect_height_2,self.rect_width_2))  
                
                pygame.display.update()
                self.ticker = time.time()  
                self.stim_time = self.ticker 
                self.bool_stimulus = True
                self.cross_flag = True
                self.elapsed_time = 0.0
                self.multi_press = False
 
            elif var == 'none':
                self._display_surf.fill((0,0,0))
                pygame.display.update()

        elif event.type == pygame.KEYDOWN:     

            if event.key == pygame.K_SPACE:      
                self.response_time = time.time()
                self.response_delay = round((self.response_time - self.stim_time)*1000,3)
                self.bool_response = True
                self.task_stat_updater()
                self.first_key_press = True

            elif event.key == pygame.K_q:
                self._running = False
    
    def on_cleanup(self):
        pygame.quit()
        self.show_stats()


    def task_stat_updater(self):
        if self.circ_flag:    
            # store data
            self.score_df = self.score_df.append({'cross_stim_time':0,'circ_stim_time':self.stim_time,
            'response_delay': self.response_delay,'correct_response': 1,'incorrect_response':0,
            'no_response':0, 'absolute_response_time': self.response_time-self.start_execution_time, 'response_type': 'green'},ignore_index = True)
            self.bool_response = False
            self.circ_flag = False
            self.multi_press=True
            print('Correct')
                
        elif self.cross_flag:
            # store data
            self.score_df = self.score_df.append({'cross_stim_time':self.stim_time,'circ_stim_time':0,
            'response_delay': self.response_delay,'correct_response': 0,'incorrect_response':1,
            'no_response':0, 'absolute_response_time':self.response_time-self.start_execution_time, 'response_type': 'red'},ignore_index = True)
            self.bool_response = False
            self.cross_flag = False
            self.multi_press=True
            print('incorrect')
        
        elif self.multi_press:
            # store mutiple-press events here
            self.score_df = self.score_df.append({'cross_stim_time':0,'circ_stim_time':0,
            'response_delay': self.response_delay,'correct_response': 0,'incorrect_response':0,
            'no_response':0, 'absolute_response_time':self.response_time-self.start_execution_time, 'response_type': 'yellow'},ignore_index = True)
            print('multi-press')
            self.multi_press_counter+=1
            self.multi_press=False
    
    def button(self,msg,x,y,w,h,ic,ac,action=None):
        
        mouse = pygame.mouse.get_pos()  
        click = pygame.mouse.get_pressed()

        if x+w > mouse[0] > x and y+h > mouse[1] > y:
            pygame.draw.rect(self._display_surf, ac,(x,y,w,h))

            if click[0] == 1 and action != None:     
                action()
                pygame.draw.rect(self._display_surf, (255,0,0),(x,y,w,h))
                self.duration_button_flag = False

        else:
            pygame.draw.rect(self._display_surf, ic,(x,y,w,h))

        smallText = pygame.font.SysFont("helvetica.ttf",20)
        textSurf, textRect = self.text_objects(msg, smallText, (0,0,0))
        textRect.center = ( (x+(w/2)), (y+(h/2)) )
        self._display_surf.blit(textSurf, textRect)

    def text_objects(self, text, font, color):
        textSurface = font.render(text, True, color)
        return textSurface, textSurface.get_rect()

    def message_display(self, text, pos_x, pos_y, font_sz):
        large_text = pygame.font.SysFont('helvetica.ttf', font_sz)
        TextSurf, TextRect = self.text_objects(text, large_text, (255,255,255))
        TextRect.center = (pos_x,pos_y)
        self._display_surf.blit(TextSurf, TextRect) 

    # so inelegant!
    def set_duration_10(self):
        self.study_duration = 10*60
        self.duration_button_flag = not(self.duration_button_flag)

    def set_duration_20(self):
        self.study_duration = 20*60
        self.duration_button_flag = not(self.duration_button_flag)

    def set_duration_30(self):
        self.study_duration = 30*60
        self.duration_button_flag = not(self.duration_button_flag)

    def set_duration_40(self):
        self.study_duration = 40*60
        self.duration_button_flag = not(self.duration_button_flag)

    def set_duration_3(self):
        self.study_duration = 3*60
        self.duration_button_flag = not(self.duration_button_flag)
    def on_execute(self):
        
        self._display_surf.fill((0,0,0))
        pygame.display.update() 
        
        self.current_time = datetime.now()
        self.dataout_file_name =  self.first_name.get_value()[:2]+self.last_name.get_value()[:2]+'_'+str(self.study_duration/60)+str(self.current_time.strftime("_%Y-%m-%d_%H-%M-%S")+'.csv')
        
        # Instructions
        self.message_display('Instructions:', self.center_x - 10,self.center_y - 100, 50)
        self.message_display('-> Hit the space-bar as soon as you see a red circle', self.center_x - 10, self.center_y - 50, 30)
        self.message_display('-> Do so ONLY when you see a red circle', self.center_x - 10, self.center_y - 20, 30)
    
        pygame.display.update()
        time.sleep(6)
        self._display_surf.fill((0,0,0))
        pygame.display.update()

        # Count Down Sequence
        countdown_elements = ['3', '2', '1', 'Go!']

        for element in countdown_elements:
            self.message_display(element, self.weight/2,self.height/2, 50)
            pygame.display.update()
            time.sleep(0.8)
            self._display_surf.fill((0,0,0))
            pygame.display.update()

        self.REFRESHEVENT = pygame.USEREVENT+1
        pygame.time.set_timer(self.REFRESHEVENT, 3000)
        self.start_execution_time = time.time()
       
        while self._running and self.execution_timer_flag:

            # Begin only if a stimulus was presented
            if self.ticker !=0:
                self.elapsed_time = time.time() - self.ticker
           
            self._display_surf.blit(self.text, self.textRect) 
            pygame.display.update()

            # if self.response_delay < self.min_response_delay:
            #     self.min_response_delay = self.response_delay

            # if self.first_key_press:
            #     self.text = self.font.render('Best: '+ str(self.min_response_delay)+' ms'+'      '+'Current: '+str(self.response_delay)+' ms', True, (255,255,255), (0,0,0))
            #     self._display_surf.blit(self.text, self.textRect) 
            #     pygame.display.update()

            if self.elapsed_time > 0.8 and self.bool_stimulus:
                
                if self.circ_flag:
                    self.score_df = self.score_df.append({'cross_stim_time':0,'circ_stim_time':self.stim_time,
                    'response_delay': np.nan ,'correct_response': 0,'incorrect_response':0,
                    'no_response':0, 'absolute_response_time': self.stim_time-self.start_execution_time, 'response_type': 'red'},ignore_index = True)
                    self.circ_flag = not(self.circ_flag)
                    self.miss_counter+=1
                    print('missed')

                elif self.cross_flag:
                    self.score_df = self.score_df.append({'cross_stim_time':self.stim_time,'circ_stim_time':0,
                    'response_delay':  np.nan,'correct_response': 0,'incorrect_response':0,
                    'no_response':1, 'absolute_response_time': self.stim_time-self.start_execution_time, 'response_type': 'black'},ignore_index = True) 
                    self.cross_flag = not(self.cross_flag)
                    print('ducked')

                
                self._display_surf.fill((0,0,0))
                pygame.display.update()
                self.bool_stimulus = False
                
                
            for event in pygame.event.get():
                self.on_event(event)

            
            self.execution_time = time.time() - self.start_execution_time
            if self.execution_time > self.study_duration:
                self.execution_timer_flag = not(self.execution_timer_flag)

        self.on_completion()    


    # graphics functions
    def autopct_generator(self,  limit):
        def inner_autopct(pct):
            return ('%.2f%%\n' % pct) if pct > limit else ''
        return inner_autopct


    def my_level_list(self, data, inlabels):
        list = []
        for i in range(len(data)):
            if (data[i]*100/np.sum(data)) > 0 : 
                list.append(inlabels[i])
            else:
                list.append('')
        return list
    
    def show_stats(self):
  
        '''
        PLOT Performance Summary Scatter Plot
        '''

        q_low = self.score_df["response_delay"].quantile(0.01)
        q_hi  = self.score_df["response_delay"].quantile(0.99)

        self.new_df = self.score_df[(self.score_df["response_delay"] < q_hi) & (self.score_df["response_delay"] > q_low)]
        self.new_df = self.new_df.dropna(subset=['response_delay'])
        # cs = CubicSpline(self.new_df['absolute_response_time'], self.new_df['response_delay'])
        x_range = np.arange(self.new_df['absolute_response_time'].min(), self.new_df['absolute_response_time'].max(), 0.5)

        spline_model = hf.get_natural_cubic_spline_model(self.new_df["absolute_response_time"], 
        self.new_df["response_delay"], minval=self.new_df['absolute_response_time'].min(),maxval=self.new_df['absolute_response_time'].max(), n_knots=3)
        y_est = spline_model.predict(x_range)

        fig, axs = plt.subplots(1, 2)
        fig.set_figheight(4.8)
        fig.set_figwidth(10,0)

        self.mean_response = self.new_df["response_delay"].mean(skipna = True)
        self.score_df.plot(kind = 'scatter',  x = 'absolute_response_time', y='response_delay', color = self.score_df['response_type'], ax = axs[0])
        axs[0].set_ylabel('Response delay (ms)')
        axs[0].set_xlabel('Time (s)')
        axs[0].set_ylim([0,800])
        
        axs[0].axhline(y=self.mean_response,linestyle= '--', color = 'b')
        axs[0].plot(x_range, y_est)

        q1 = self.score_df['response_delay'].quantile(0.25)
        q3 = self.score_df['response_delay'].quantile(0.75)
        IQR = q3-q1

        axs[0].fill_between(self.score_df['absolute_response_time'], self.mean_response - IQR, self.mean_response + IQR, alpha=0.2)
        axs[0].grid()
       
        outer_labels = ['Good', 'Bad']
        inner_labels = ['Right','Duck','Dummy','Wrong','Lapse', 'Multi']

        size = 0.3

        #TODO: define unique vals array for both pie charts
        vals_1 = np.array([self.score_df['correct_response'].sum()+self.score_df['no_response'].sum(),self.score_df['incorrect_response'].sum()+self.miss_counter+ self.multi_press_counter])
        vals_2 = np.array([self.score_df['correct_response'].sum(),self.score_df['no_response'].sum(),0.0,self.score_df['incorrect_response'].sum()+self.miss_counter,self.multi_press_counter])      

        a,b = [plt.cm.Greens, plt.cm.Reds]

        dummy, texts, autopcts = axs[1].pie(vals_1, radius=1, colors=[a(0.6), b(0.6)],
            wedgeprops=dict(width=size, edgecolor='w'), labels = self.my_level_list(vals_1, outer_labels), autopct= self.autopct_generator(0),pctdistance = 0.9, labeldistance=1.1, startangle = 40)
        # plt.setp(autopcts, **{'weight':'bold', 'fontsize':10.5})
        plt.setp(texts, **{'weight':'bold', 'fontsize':12.5})


        axs[1].pie(vals_2, radius=1-size, colors=[a(0.4), a(0.2), b(0.45), b(0.3), b(0.15)],
            wedgeprops=dict(width=size, edgecolor='w'), labels = self.my_level_list(vals_2, inner_labels), labeldistance = 0.7, startangle = 40)

        axs[1].set(aspect = 'equal')
        fig.suptitle('Participant Response Summary', fontsize=16)

        fig.savefig(self.absolute_file_path+self.first_name.get_value()[:2]+self.last_name.get_value()[:2]+'_'+str(self.study_duration/60)+str(self.current_time.strftime("_%Y-%m-%d_%H-%M-%S")+'.png'))
        plt.show()
        

    def on_completion(self):
        self._display_surf.fill((0,0,0))
        pygame.display.update()
        self.message_display('Experiment Completed', self.weight/2,self.height/2, 50)
        pygame.display.update()
        time.sleep(0.8)
        self._display_surf.fill((0,0,0))
        pygame.display.update()
        time.sleep(0.8)
        self.message_display('..saving your data..', self.weight/2,self.height/2, 40)
        pygame.display.update()
        time.sleep(0.8)
        self.score_df.to_csv(self.absolute_file_path+self.dataout_file_name, sep = ',', encoding = 'utf-8')

        self.on_cleanup()
    
    
    def on_intro(self): 

        intro = True
        if self.on_init() == False:
            self._running = False

        self.title_element = thorpy.make_text("Input Detail Below", 18 , (0,0,0))
        self.first_name = thorpy.Inserter(name="First Name: ", value="")
        self.last_name = thorpy.Inserter(name="Last Name: ", value="")

        self.box = thorpy.Box(elements=[self.title_element, self.first_name, self.last_name])
        # #we regroup all elements on a menu, even if we do not launch the menu
        self.menu = thorpy.Menu(self.box)
        self.box.fit_children()
        #important : set the screen as surface for all elements
        for element in self.menu.get_population():
            element.surface = self._display_surf
        
        # #use the elements normally...
        self.box.set_center((310,100))
        self.box.blit()
        self.box.update()
        self.box.set_main_color((220,220,220,180))
        
        while intro:
            events = pygame.event.get()
            for event in events:
                if event.type == pygame.QUIT:
                    pygame.quit()
                    quit()
                self.menu.react(event)
            self.button("Begin",275,300,75,50,(255,255,255),(0,100,255), self.on_execute)
            
            
            self.message_display('Duration (mins)', 310, 200, 30)
            if self.duration_button_flag:
                self.button("3",  180,230,40,40,(255,255,255),(0,100,255), self.set_duration_3)
                self.button("10", 240,230,40,40,(255,255,255),(0,100,255), self.set_duration_10)
                self.button("20", 300,230,40,40,(255,255,255),(0,100,255), self.set_duration_20)
                self.button("30", 360,230,40,40,(255,255,255),(0,100,255), self.set_duration_30)
                self.button("40", 420,230,40,40,(255,255,255),(0,100,255), self.set_duration_40)
    
            pygame.display.update()
                  
if __name__ == '__main__':
    theApp = App()
    theApp.on_intro()


 