import matplotlib.pyplot as plt
import pandas as pd
import os
import glob
from fnirs_helper_functions import *

file_path = '/home/rohith/Insync/rohithkarthikeyan@tamu.edu/Google Drive/Resources/R15-data/HT-Organized/'

''' set working directory '''
os.chdir(file_path)

fnirs_files, performance_files, probe_map, features_df, perf_df = initializer()

del_t = 15 
channel_list = ['A1', 'A2', 'B3', 'B4', 'B5' , 'B6'
 ,'C4', 'C6' ,'C7','D6' ,'D7' ,'D8' ,'D21', 'E1' ,'E17'
  ,'F18', 'F19', 'F20', 'F21', 'G8', 'G19', 'G21','H5', 'H6','H20', 'H21' ]
signal_type =  ['HBO', 'HBR', 'HBT']
domain = 'time'

''' get complete df
'''

# i =0
# j = 0
# k = 0
# l=0

# for f in fnirs_files:
    
#     fnirs_data = pd.read_csv(f)
#     if len(fnirs_data.columns)== 96:
#         i+=1
#     elif len(fnirs_data.columns)== 93:
#         print('93: ', f)
#         j+=1
#     elif len(fnirs_data.columns) == 97:
#         k+=1
#     else:
#         l+=1
#         print(len(fnirs_data.columns))
#         print('Other: ', f)

# print('96: ', str(i))
# print('93: ', str(j))
# print('97: ', str(k))
# print('other:', str(l))

# input('----')

complete_df = feature_df_bridge(fnirs_files, features_df, 
channel_list, probe_map, file_path,signal_type, del_t, domain)

complete_df.to_csv('INTERMEDIATE_DF.csv', index = False)

''' USE FEATURES DF FOR SELECTIVE PLOTTING
'''
output_df = create_wide_df(complete_df)
output_df.to_csv('FINAL_OUTPUT.CSV',index=False)

# feature_viz(fnirs_files[0][19:-8], channel_list, complete_df, 'HBT', modifier = 'channels')
# feature_viz(fnirs_files[0][19:-8], channel_list, complete_df, signal_type, modifier = 'signals')