% Convert all files to CSV and organize-by-channel
clc; clear all; close all;

%% read each processed file, and save as csv
load('/home/rohith/Insync/rohithkarthikeyan@tamu.edu/Google Drive - Shared drives/tDCS: Closed Loop Neurotech/Data/[Pilot] Prediction Modeling/PVT/instruments/fNIRS/YiZh/fNIRS/test.mat');
out_data = procResult.dc;
new_data = [];

for i = 1:21
    new_data = horzcat(new_data, out_data(:,:,i));
end


new_data = horzcat(new_data, s);
new_data = horzcat(new_data, t);

writematrix(new_data, 'fnirs.txt')
writematrix(SD.MeasList,'probemap.csv')
