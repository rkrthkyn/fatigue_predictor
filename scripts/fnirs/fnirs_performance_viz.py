import matplotlib.pyplot as plt
import pandas as pd
import os
import glob
from fnirs_helper_functions import *

file_path = '/home/rohith/Insync/rohithkarthikeyan@tamu.edu/Google Drive - Shared drives/tDCS: Closed Loop Neurotech/Data/[Pilot] Prediction Modeling/PVT/instruments/fNIRS/YiZh/'
# file_path = '/home/rohith/Insync/rohithkarthikeyan@tamu.edu/Google Drive/Resources/R15-data/pipeline-test/'

''' set working directory '''
os.chdir(file_path)

fnirs_files, performance_files, probe_map, _, perf_df = initializer()
del_t = 30

'''
Specify channel ID here
'''
channel_list = ['C1', 'A3','E5']

'''
analysis domain
'''

domain = 'time'

if domain == 'time':
    ''' init plot object '''
    fig, axs = plt.subplots(len(channel_list), 5, figsize = (10,4), sharex = True, sharey = True) #TODO: columns here reflect feature count
    cols = ['MEAN', 'VAR', 'MAX', 'KURT', 'SKEW']
    features_df = pd.DataFrame(columns = ['mean', 'var', 'peak', 'kurt', 'skew', 'time'])


for f in fnirs_files:
    
    k =0 

    for channel in channel_list:

        channel_idx = get_channel_idx(channel[0], int(channel[1]) , file_path+probe_map[0], signal_type = 'HBT')

        ''' get fnirs data in a nice format '''
        channel_data = assemble_fnirs_data(f, channel_idx)     
        features_df = extract_fnirs_features(channel_data,features_df, del_t, domain)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
        
        ''' Plotting stuff 
        Functionified!
        '''
        j = 0
        for (column_name, column_data) in features_df.iteritems():
            if column_name != 'time' and j<=4:
                plot_feature_data(axs[k,j], column_data, column_name, features_df)
                j+=1
        
        k+=1    

        '''
        Reset your variables
        '''
        if domain == 'time':
            features_df = pd.DataFrame(columns = ['mean', 'var', 'peak', 'kurt', 'skew', 'time'])
        
        perf_df = pd.DataFrame(columns = ['delay','std', 'time_stamp'])


    
    props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)

    for ax, col in zip(axs[0], cols):
        ax.set_title(col, size = 'large',bbox=props)

    for ax, row in zip(axs[:,0], channel_list):
        ax.set_ylabel(row, rotation=0, size='large', labelpad = 1.5,  bbox=props)

    # fig.set_figwidth(25.0)
    fig.suptitle('Time domain fNIRS feature summary', fontsize=16)
    plt.show()

   