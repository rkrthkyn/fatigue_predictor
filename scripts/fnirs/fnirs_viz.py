import matplotlib.pyplot as plt
import pandas as pd
import os
import glob
from fnirs_helper_functions import *

# file_path = '/home/rohith/Insync/rohithkarthikeyan@tamu.edu/Google Drive - Shared drives/tDCS: Closed Loop Neurotech/Data/[Pilot] Prediction Modeling/PVT/instruments/EEG-fNIRS/YiZh/fNIRS-channelwise/'
file_path = '/home/rohith/Insync/rohithkarthikeyan@tamu.edu/Google Drive/Resources/R15-data/pipeline-test/'

''' set working directory '''
os.chdir(file_path)

fnirs_files, performance_files, probe_map, features_df, perf_df = initializer()
del_t = 2

'''
Specify channel ID here
Ensure to make changes for HBO/HBR/HBT
'''

channel_idx = get_channel_idx('B', 4 , file_path+probe_map[0], signal_type = 'HBT')

for f in fnirs_files:
    
    ''' get fnirs data in a nice format '''
    channel_data = assemble_fnirs_data(f, channel_idx)

    ''' get stim-times from channel data '''
    stim_fc1 = channel_data.loc[channel_data['fc1']==1]['time'].copy()
    stim_fc1 = stim_fc1.reset_index()
    s1 = stim_fc1['time']

    stim_fc2 = channel_data.loc[channel_data['fc2']==1]['time'].copy()
    stim_fc2 = stim_fc2.reset_index()
    s2 = stim_fc2['time']

    ''' organize stim into df '''
    stim_df = pd.DataFrame()
    stim_df['s1'] = s1
    stim_df['s2'] = s2

    ''' init plot object '''
    fig, axs = plt.subplots(len(stim_df.columns), 5)
    k =0 
    
    for column in stim_df:
        
        for i in range(0,len(stim_df.index)-1):

            current_df = pd.DataFrame(columns = ['mean', 'var', 'peak', 'kurt', 'skew', 'time'])  
        
            ''' partition fnirs data by stim intervals, plot '''
        
            # if i != len(stim_df.rows)-1:
            #     interval = channel_data['time'].between(stim_df[column].iloc[i], stim_df[column].iloc[i+1])
            # else:
            #     interval = channel_data['time'].between(stim_df[column].iloc[i], channel_data['time'].iloc[-1])

            interval = channel_data['time'].between(stim_df[column].iloc[i], stim_df[column].iloc[i+1])
            interval_data = channel_data[interval]

            current_df = extract_fnirs_features(interval_data, current_df, del_t, 'time')                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
            current_df['norm_time'] = feature_scaling(current_df['time'], range = (0,30))

            ''' append features to df '''
            features_df = features_df.append(current_df, sort = False)
                      
        '''
        CLEANUP HERE IF FUBAR
        average all points that lie within a certain interval, for cleaner plots
        '''

        mod_df = pd.DataFrame(columns = ['mean', 'var', 'peak', 'kurt', 'skew', 'time'])

        start = 0
        del_t_current = 2
        stop = start+del_t_current
        end_of_df=  False

        while not(end_of_df):
        
            if stop > features_df['norm_time'].iloc[-1]:
                stop = features_df['norm_time'].iloc[-1]
                end_of_df = True
                print('end-of-df')

            interval = features_df['norm_time'].between(start,stop, inclusive = True)
            
            ''' 
                do analysis on interval data provide time stamp as median 
                - Time domain features (SDNN, pNN50, RMSSD)
                - Frequency domain features (LF, HF, LF/HF)
                - Non-linear domain fe  atures (sd1, sd2, sampen)
            '''  

            mod_interval_data = features_df[interval]
        
            if len(mod_interval_data.index) < 5:
                end_of_df = True
                print('forced end-of-df')
            else:   
                mod_df = mod_df.append({'mean':mod_interval_data['mean'].mean(),'peak': mod_interval_data['peak'].mean(),
                'var': mod_interval_data['var'].mean(),'kurt': mod_interval_data['kurt'].mean(), 'skew':mod_interval_data['skew'].mean(), 
                'time': mod_interval_data['norm_time'].median()},ignore_index = True)

            start = stop 
            stop = start+del_t_current
        
        '''
        =====================================================
        '''

        ''' Plotting stuff 
        Functionified!
        '''

        j = 0
        for (column_name, column_data) in mod_df.iteritems():
            if column_name != 'time' and j<=4:
                plot_feature_data(axs[k,j], column_data, column_name, mod_df)
                j+=1
            
        '''
        Reset your variables
        '''
        features_df = pd.DataFrame(columns = ['mean', 'var', 'peak', 'kurt', 'skew', 'time'])
        perf_df = pd.DataFrame(columns = ['delay','std', 'time_stamp'])
        k+=1


    fig.set_figwidth(25.0)
    fig.suptitle('Participant fNIRS Feature Summary', fontsize=16)
    plt.show()

   