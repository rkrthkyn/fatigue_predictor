library(caretEnsemble)
library(doParallel)
library(tidyverse)
library(caret)

# Set working directory
setwd("/home/rohith/Insync/rohithkarthikeyan@tamu.edu/Google Drive/Resources/R15-data/HT-Organized")

# Import dataset
input_df <- read.csv(file = 'FINAL_OUTPUT_LEG.csv')
copy_of_input <- input_df
# Determine correlation across all features
copy_of_input$condition <- NULL
copy_of_input$id <- NULL
copy_of_input <- na.omit(copy_of_input)
correlationMatrix <- cor(copy_of_input,  use = "complete.obs")
highlyCorrelated <- findCorrelation(correlationMatrix, cutoff=0.85)

# remove highly correlated variables
highlyCorrelated <- sort(highlyCorrelated)
reduced_data <- copy_of_input[,-c(highlyCorrelated)]

# Split into Train, Test
set.seed(123)
part.index <- createDataPartition(input_df$condition, 
                                  p = 0.75,                         
                                  list = FALSE)

# Subset the INPUT DF (E.g. consider only MEAN, VAR under HBT/O for this first trial)
X <- reduced_data[!grepl("HBR", names(reduced_data))]
X <- copy_of_input
input_df$condition <- as.character(input_df$condition)
input_df$condition[input_df$condition == "no-stress"] <- "nostress"
colnames(X)
Y <- input_df$condition

# Partition data into test, train
x_train <- X[part.index,]
x_test <- X[-part.index,]
y_train <- Y[part.index]
y_test <- Y[-part.index]

# Setup parallelization
registerDoParallel(4)
getDoParWorkers()
set.seed(123)
my_control <- trainControl(method = "cv", number = 5, 
                           savePredictions = "final",
                           index = createFolds(y_train, 5),
                           allowParallel = TRUE, 
                           classProbs = TRUE)

# Train multiple models
set.seed(222)
model_list <- caretList(x_train,
                        y_train,
                        trControl = my_control,
                        methodList = c("adaboost","svmLinear", "rf", "knn", "nb"),
                        tuneList = NULL,
                        continue_on_fail = FALSE)

model_list$adaboost$results
model_list$svmLinear$results
model_list$rf$results
model_list$knn$results
model_list$nb$results

plot(varImp(model_list$adaboost), top=20, main = 'ADA')
plot(varImp(model_list$svmLinear), top=20, main = 'SVML')
plot(varImp(model_list$rf), top=20, main = 'RF')
plot(varImp(model_list$knn), top=20, main = 'kNN')
plot(varImp(model_list$nb), top=20, main = 'NB')

# Observe resuts
options(digits = 3)
model_results <- data.frame(
        AB = min(model_list$adaboost$results$Accuracy),
        SVMR = min(model_list$svmLinear$results$Accuracy),
        RF = min(model_list$rf$results$Accuracy),
        kNN = mean(model_list$knn$results$Accuracy),
        NB = mean(model_list$nb$results$Accuracy))
print(model_results)

# Resample models observe results
resamples <- resamples(model_list)
dotplot(resamples, metric = "Accuracy")

# Check for correlation in each method
modelCor(resamples)

# Try an ensemble model
set.seed(222)
ensemble_1 <- caretEnsemble(model_list, 
                            metric = "Accuracy", 
                            trControl = my_control)
summary(ensemble_1)

# Plot results ensemble
plot(ensemble_1)

# PREDICTIONS
pred_ab <- predict.train(model_list$adaboost, newdata = x_test)
pred_svm <- predict.train(model_list$svmLinear, newdata = x_test)
pred_rf <- predict.train(model_list$rf, newdata = x_test)
pred_knn <- predict.train(model_list$knn, newdata= x_test)
pred_nb <- predict.train(model_list$nb, newdata = x_test)
predict_ens1 <- predict.train(ensemble_1, newdata = x_test)

confMatAB <- table(y_test,pred_ab)
confMatSVM <- table(y_test,pred_svm)
confMatRF <- table(y_test,pred_rf)
confMatKNN <- table(y_test, pred_knn)
confMatNB <- table(y_test, pred_nb)
confMatEns1 <- table(y_test, predict_ens1)

# ACCURACY
pred_accuracy <- data.frame(AB = sum(diag(confMatAB))/sum(confMatAB),
                            SVML = sum(diag(confMatSVM))/sum(confMatSVM),
                            RF = sum(diag(confMatRF))/sum(confMatRF),
                            KNN = sum(diag(confMatKNN))/sum(confMatKNN),
                            NB = sum(diag(confMatNB))/sum(confMatNB),
                            ENS = sum(diag(confMatEns1))/sum(confMatEns1))

print(pred_accuracy)

# Try a neural network
set.seed(123)
nnetFit <- train(x_train, 
                 y_train,
                 method = "nnet",
                 metric = "ROC",
                 trControl = my_control,
                 verbose = FALSE, 
                 maxit = 1000)

nnetFit$results
ggplot(nnetFit)
varImp(nnetFit)
pred_nnet <- predict.train(nnetFit,newdata = x_test) 
confMatNnet <- table(y_test,pred_nnet)
nnet_accuracy <- sum(diag(confMatEns1))/sum(confMatEns1)

# try RFE
rfFuncs$summary <- twoClassSummary

ctrl <- rfeControl(functions = rfFuncs, 
                   method = 'cv',
                   returnResamp = TRUE,
                   number = 2,
                   verbose = FALSE, 
                   savePredictions = true)

profiler <- rfe(x_train, 
               factor(y_train), 
                sizes = c(2:10, 5, 20, 40, 60,100), 
                method = 'nnet',
                tuneGrid = expand.grid(size=c(2,4,10), decay=c(0.1)), 
                maxit = 1000,
                metric = 'ROC', 
                rfeControl = ctrl) 

profiler$results



library(pROC)
y_pred <- predict(profiler, x_test)
pROC_obj <- roc(y_test,
                y_pred$posterior,
                smoothed = TRUE,
                # arguments for ci
                ci=TRUE, ci.alpha=0.9, stratified=FALSE,
                # arguments for plot
                plot=TRUE, auc.polygon=TRUE, max.auc.polygon=TRUE, grid=TRUE,
                print.auc=TRUE, show.thres=TRUE)

