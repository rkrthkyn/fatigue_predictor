#!/usr/bin/env
from hrvanalysis import remove_outliers, interpolate_nan_values, remove_ectopic_beats
from hrvanalysis import extract_features
import matplotlib.pyplot as plt
import pandas as pd
import os
from hrv_helper_functions import initializer, extract_hrv_features, get_response_data, feature_scaling, fit_reg_spline, plot_feature_data

'''
- Show stim interval
- Setup to work irrespective of stim condition
'''

'''
Extract time-domain, frequency-domain, and non-linear features from IBI data
- Remove outliers
- Remove ectopic beats
- Interpolate for NAN values
'''

''' set required variables here 
- point file path to desired folder on your devicer
'''

file_path = '/home/rohith/Insync/rohithkarthikeyan@tamu.edu/Google Drive - Shared drives/tDCS: Closed Loop Neurotech/Data/[Pilot] Prediction Modeling/PVT/duration/40/'

''' set working directory '''
os.chdir(file_path)

# iterate through ibi data files
# TODO: file matching highly dependent on self-encoded file name - needs to be easier

def main():
    
    ibi_files, performance_files, features_df, perf_df = initializer()
    del_t = 300


    for f in ibi_files:

        stim = False
        sham = False
        condition = f[5:8]
        
        if condition == 'ACT':
            stim = True
        elif condition == 'PRE':
            condition = 'PRE'
        elif condition == 'REV':
            stim = True
        elif condition == 'SHA':
            sham = True
        else:
            condition = 'NUL'


        # extract features and performance data
        features_df, performance_data, ibi_new, perf_df = extract_hrv_features(f, features_df, performance_files, perf_df, del_t)
        
        # apply filters to response delay data
        filtered_time, filtered_delay = get_response_data(performance_data, True)

        # feature scaling for response delay and performance data
        filtered_delay_scaled = feature_scaling(filtered_delay)
        ibi_scaled = feature_scaling(ibi_new['nn_intervals'])

        fig, axs = plt.subplots(3, 3,  sharex=True)
        # regularized cubic spline for performance
        x_range_perf, y_est_perf = fit_reg_spline(filtered_time, filtered_delay_scaled)


        ''' Plotting stuff 
        Functionified!
        '''
        i = 0
        j = 0
        for (column_name, column_data) in features_df.iteritems():
            if column_name != 'feature_time_stamp' and i<=2:
                plot_feature_data(axs[i,j], column_data, column_name, features_df, x_range_perf, y_est_perf)
                if stim == True:
                    rect = plt.Rectangle((60*23, -1), 300, 1000, color='r', alpha=0.2)
                    axs[i,j].text(60*23+50, min(y_est_perf)+5, "STIM")
                    axs[i,j].add_patch(rect)
                elif sham == True:
                    axs[i,j].text(60*23, min(y_est_perf)+5, "STIM")
                    axs[i,j].axvline(x = 60*23)
                if j < 2:
                    j+=1
                else:
                    j = 0
                    i+=1


        fig.set_figheight(15.0)
        fig.set_figwidth(15.0)
        fig.suptitle('['+condition+'] '+'Participant HRV Feature Summary', fontsize=16)
        
        plt.show()

        # regularized cubic spline
        x_range_hrv, y_est_hrv = fit_reg_spline(ibi_new['time'], ibi_scaled)
    
        plt.scatter(ibi_new['time'].values, ibi_scaled, color = '#f39c12', alpha = 0.5, label = 'IBI', marker = 'P')
        plt.scatter(filtered_time, filtered_delay_scaled, color = '#16a085', alpha = 0.5 , label = 'Response delay')
        
        plt.plot(x_range_perf,y_est_perf, color = '#2980b9', linewidth = '3', label = 'Response trend', linestyle = '--')
        plt.plot(x_range_hrv,y_est_hrv, color = '#d35400', linewidth = '3', label = 'IBI trend', linestyle = '--')
        plt.xlabel('Time (s)')
        
        plt.ylabel('Normalized range')
        plt.title('Normalized HRV and performance summary')
        plt.legend()
        plt.show()

        # reset your vars
        features_df = pd.DataFrame(columns = ['sdnn', 'pnn50', 'rmssd', 'lf', 'vlf', 'hf', 'lf_hf_ratio', 'sd1', 'sd2', 'samp_en', 'feature_time_stamp'])
        perf_df = pd.DataFrame(columns = ['delay','std' ,'time_stamp'])

if __name__ == "__main__":
    main()