#!/usr/bin/env
from hrvanalysis import remove_outliers, interpolate_nan_values, remove_ectopic_beats
from hrvanalysis import extract_features
import matplotlib.pyplot as plt
import pandas as pd
import os
import numpy as np
from hrv_helper_functions import initializer, extract_hrv_features, get_response_data, feature_scaling, fit_reg_spline, plot_feature_data   

'''
- Constrain window size for plot
'''

''' set required variables here 
- point file path to desired folder on your devicer
'''

file_path = '/home/rohith/Insync/rohithkarthikeyan@tamu.edu/Google Drive - Shared drives/tDCS: Closed Loop Neurotech/Data/[Pilot] Prediction Modeling/PVT/instruments/tDCS/RoKa-2/'
''' set working directory '''
os.chdir(file_path)

stim_time = 23

def main():

    ibi_files, performance_files, features_df, perf_df = initializer()
    del_t = 100
    fig, axs = plt.subplots(6, 1,  sharex=True, figsize=(6, 12))
    error_count = []
    labels = []
    
    for f in ibi_files:

        features_df, performance_data,_, perf_df = extract_hrv_features(f, features_df, performance_files, perf_df, del_t)
        
        # apply filters to response delay data
        filtered_time,filtered_delay = get_response_data(performance_data, False)

        # regularized cubic spline for performance
        x_range_perf, y_est_perf = fit_reg_spline(filtered_time, filtered_delay)

        condition = f[5:8]
        if condition == 'ACT':
            label = 'ACTIVE'
            axis = axs[0]
        elif condition == 'PRE':
            label = 'PRE-STIM'
            axis = axs[2]
        elif condition == 'NUL':
            label = 'NULL'
            axis = axs[4]
            axis.set_xlabel('time (s)')
        elif condition == 'REV':
            label = 'REVERSED'
            axis = axs[1]
        elif condition == 'SHA':
            label = 'SHAM'
            axis = axs [3]


        # plot response trend
        axis.plot(x_range_perf,y_est_perf, color = '#2980b9', linewidth = '3', label = 'Response trend', linestyle = '--', alpha= 0.35)
        axis.text(20, max(y_est_perf)-5, label)
        axis.scatter(perf_df['time_stamp'].values, perf_df['delay'].values,color = '#16a085', alpha = 0.5)
        # axis.errorbar(perf_df['time_stamp'].values, perf_df['delay'].values, yerr = perf_df['std'].values)

        if f[5:8] != 'PRE' and f[5:8] != 'NUL' and f[5:8] != 'SHA':
            rect = plt.Rectangle((60*stim_time, 100), 300, 1000, color='r', alpha=0.2)
            axis.text(60*stim_time+50, min(y_est_perf)+5, "STIM")
            axis.add_patch(rect)
        elif f[5:8] == 'SHA':
            axis.text(60*stim_time, min(y_est_perf)+5, "STIM")
            axis.axvline(x = 60*stim_time)
        
        axis.set_ylim([320,500])
        # axis.set_ylim([300,450])
        # reset your vars

        features_df = pd.DataFrame(columns = ['sdnn', 'pnn50', 'rmssd', 'lf', 'vlf', 'hf', 'lf_hf_ratio', 'sd1', 'sd2', 'samp_en', 'feature_time_stamp'])
        perf_df = pd.DataFrame(columns = ['delay','std' ,'time_stamp'])
        error_count.append(performance_data['incorrect_response'].sum(axis=0))
        labels.append(label)

    
    axis = axs[5]
    axis.remove()
    axis = fig.add_subplot(616)
    ypos = np.arange(len(error_count))     # the label locations
    width = 0.35                           # the width of the bars
    axis.barh(ypos - width/2, error_count, width)
    axis.set_yticks(ypos)
    axis.set_yticklabels(labels)
    axis.set_xlim([0,25])
    axis.set_xlabel('No. of incorrect responses')
    fig.suptitle('Response time (ms) under different stimulation conditions', fontsize=12)      
    plt.show()

if __name__ == "__main__":
    main()